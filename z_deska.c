/*****************************************************
This program was produced by the
CodeWizardAVR V2.03.7 Standard
Automatic Program Generator
� Copyright 1998-2008 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project : z�kladn� deska
Version : v_10    odpoved,prostup, 10 bit p�evodn�k
Date    : 23.4.2013
Author  : Vit Baloch                      
Company : Elsvo Most sro                  
Comments: 


Chip type           : ATmega32
Program type        : Application
Clock frequency     : 12,000000 MHz
Memory model        : Small
External RAM size   : 0
Data Stack size     : 512
*****************************************************/

#include <mega32.h>
#include <z_deska.h>
#include <stdio.h>

// I2C Bus functions
#asm
   .equ __i2c_port=0x12 ;PORTD
   .equ __sda_bit=6
   .equ __scl_bit=4
#endasm
#include <i2c.h>

// PCF8583 Real Time Clock functions
#include <pcf8583.h>

void prev_t(unsigned char *kam,unsigned char *odkud,unsigned char pocet_bit);
//unsigned char  pocet_1 (unsigned char *pole,unsigned char pocet_bit);   

#define RXB8 1
#define TXB8 0
#define UPE 2
#define OVR 3
#define FE 4
#define UDRE 5
#define RXC 7

#define FRAMING_ERROR (1<<FE)
#define PARITY_ERROR (1<<UPE)
#define DATA_OVERRUN (1<<OVR)
#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)

// USART Receiver buffer
#define RX_BUFFER_SIZE 15
char rx_buffer[RX_BUFFER_SIZE+12];

// USART Transmitter buffer
#define TX_BUFFER_SIZE 17
char tx_buffer[TX_BUFFER_SIZE+10];

#if TX_BUFFER_SIZE<256
unsigned char tx_wr_index,tx_rd_index,tx_counter;
#else
unsigned int tx_wr_index,tx_rd_index,tx_counter;
#endif

#if RX_BUFFER_SIZE<256
unsigned char rx_wr_index,rx_rd_index,rx_counter;
#else
unsigned int rx_wr_index,rx_rd_index,rx_counter;
#endif

//
#include <BHO.h> 
//       

unsigned char pol[30],apol[30],inpol[30],stav[20],pomo_pol[20],pripol[30],vysl[20];
unsigned char wait,cykl,maska,suma,ukaz_pinu,ukaz_byte,pobit,cteni_zaznamu;
unsigned char ksuma,control,i,j,k,wait_prenos,cykl_prenos,sekunda,pocet_zprav;
unsigned char sm_gong,cykl_tel,cas_100ms,uschov_serial,pomo;
unsigned char wait_control,xwait,wait2,cas_telefonu,po_sec,cas_gsm;
unsigned char periferie,cas_time_out,b_doba_provozu,sm_gongtest;
unsigned char servis,end_cykl,tl2,vstup,start,ukaz;
unsigned char vst_maska,vyst_maska,ukaz_vst,ukaz_vyst,cas_pocet_zprav,gsm_control;
unsigned char wait_eeprom,control_odpoved,amaska,cas_vypnuti,cas_obnova;
unsigned char posl_cislo_hlasice,zapis_zaznamu,cas_blok_zpravy,pomo1,control_ach;
unsigned char wait5,binary,wait_binary,posun,schov,cas_wd,tel_cas_0;
unsigned char pol_tel[30],ukaz_tel,pocet_prvni_puls,cas_telefon;
unsigned char aaa,bbb,ccc,cas_blik_zprava,control_audio,cas_control_audio;

unsigned char  aa,bb,cc,dd,ee,ff;
 
//unsigned char pomo1,pomo2,pomo3;

//RTC
unsigned char hour,min,sec,date,month,year_uch,year;
unsigned char ho,mi,se,hs,da,mo;
unsigned int ye;

unsigned int cas,doba_vysilani,cas_nouze,cas_timeout_kontrola,cas_kontroly,doba_provozu;
unsigned int wait3,wait4,pointr_sinus,cas_obnovy,b_prisla_odpoved;

bit zprava_485,led,zmena,prijem,je_mz,blok_nul,zprava,chyba,je_zprava;
bit b_doba_vysilani,je_ext,je_mikrofon,set_motorola,xzmena,sek;
bit set_test,konec_vysilani,je_znelka,je_drat,b_cas_vypnuti;
bit test_kontrola, b_time_out,b_restart,povol_pwm,b_pocet_zprav,set_100ms,set_odpoved;
bit b_cas_blok_zpravy,pc_hovor,b_cas_wd;
bit tel_zm1,tel_zm0,tel_zm_vst,tel_zm_star,tel_zm_por,byl_znak,blik_zprava,led1;

unsigned char xpol[600];
unsigned int ukaz_xpol;
 
struct 
	{
   unsigned char vst;
   unsigned char star;
   unsigned char pom;
   unsigned char stav;
   unsigned char zm1;
   unsigned char zm0;
   unsigned char min_stav;
   } in[4];

  struct
   {          
   unsigned char cislo_hlasice;
   unsigned char typ;
   unsigned char stav; 
   unsigned char cislo_hlasice_prijate;
   unsigned char byte1;
   unsigned char byte2;
   }
   gsm_ram;   
     
eeprom unsigned char E_zapis_zaznamu,E_cteni_zaznamu;

eeprom struct
   {          
   unsigned char cislo_hlasice;
   unsigned char typ;
   unsigned char stav;        
   }
   gsm_eeprom;
   
eeprom  struct
//struct
   {
    unsigned char cislo_hlasice;
    unsigned char hod;
    unsigned char min;
    unsigned char sec;
    unsigned char den;
    unsigned char mesic;
    unsigned char rok;
    unsigned char byte1;
    unsigned char byte2;
    } zaznam[MAX_ZAZNAM];       
                                  
   extern flash unsigned char sinus[];           
 
//----------------------------------------------

void zmena_casu(void);

void serial (unsigned char param1,unsigned char param2,unsigned char param3) 
{		
   stav[status1] |= ready;
 
   tx_buffer[p_STX]=STX;
	tx_buffer[p_ADRESA_KAM]=param1;
	tx_buffer[p_ADRESA_ODKUD]=Z_DESKA;
	tx_buffer[p_DATA1]=param2;
	tx_buffer[p_DATA2]=param3;  
   tx_buffer[p_DATA3]=stav[povoleni];
	tx_buffer[p_DATA4]=stav[status1];
   tx_buffer[p_DATA5]=stav[status2];
	tx_buffer[p_DATA6]=stav[rezerva];
   tx_buffer[p_DATA7]=stav[stav_servis];
	tx_buffer[p_ETX]=ETX;
   tx_buffer[p_KSUMA]=0;
   
	for (j=0;j<=p_ETX;j++)
		tx_buffer[p_KSUMA]+=tx_buffer[j];
	tx_buffer[p_KSUMA]= (~tx_buffer[p_KSUMA])+1;
   cykl_prenos=1;
     
   stav[status2] = 0;
    
     wait_prenos=150;
     while (wait_prenos);
     
     wait_prenos=15;
     while (wait_prenos)
      {
      if (prijem)
         {
         prijem=0;
         }
      else
         {
         tx_rd_index=1;
         tx_counter=16;  
         RTS=1;   
         UDR = tx_buffer[0];
         break;
         }   
      }    
     NOP; 
}
//

void serial2(void)
   {  
   wait_prenos=100;
   while (wait_prenos);
   
   for (j=0;j<=p_ETX;j++)
		tx_buffer[p_KSUMA]+=tx_buffer[j];
	tx_buffer[p_KSUMA]= (~tx_buffer[p_KSUMA])+1;
   cykl_prenos=1;
   
   wait_prenos=150;
   while (wait_prenos);
     
   wait_prenos=15;
   while (wait_prenos)
      {
      if (prijem)
         {
         prijem=0;
         }
      else
         {
         tx_rd_index=1;
         tx_counter=16;  
         RTS=1;   
         UDR = tx_buffer[0];
         break;
         }
      }   
   }

//
  void serial1 (unsigned char param) 
{		
   //for (j=0;j<=p_ETX;j++)
     // tx_buffer[j]=vypol[j];   
 
   tx_buffer[p_STX]=STX;              
   tx_buffer[p_ADRESA_KAM]= PC;
	tx_buffer[p_ADRESA_ODKUD]=DESKA_ODPOVED;  
   tx_buffer[p_DATA1]=param;  
	tx_buffer[p_ETX]=ETX;
   tx_buffer[p_KSUMA]=0;
   
	for (j=0;j<=p_ETX;j++)
		tx_buffer[p_KSUMA]+=tx_buffer[j];
	tx_buffer[p_KSUMA]= (~tx_buffer[p_KSUMA])+1;
   cykl_prenos=1;
     
   stav[status2] = 0;
     
   wait_prenos=150;
   while (wait_prenos);
     
   wait_prenos=20;
   while (wait_prenos)
      {
      if (prijem)
         {
         prijem=0;
         }
      else
         {
         tx_rd_index=1;
         tx_counter=16;  
         RTS=1;   
         UDR = tx_buffer[0];
         break;
         }   
      }  
     NOP; 
}

//----------------------------------------------

// USART Receiver interrupt service routine
interrupt [USART_RXC] void usart_rx_isr(void)
{
char status,data;
status=UCSRA;
data=UDR;
prijem=1;
if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
   {
    b_time_out=1;
    cas_time_out=4;   
    b_restart = 0;
   
   rx_buffer[rx_wr_index]=data;
   if (++rx_wr_index == RX_BUFFER_SIZE) 
      rx_wr_index=0;
   if (++rx_counter == RX_BUFFER_SIZE)
      {
      rx_counter=0;
      rx_wr_index=0;
      zprava_485 = 1;
      };
   };
}

#ifndef _DEBUG_TERMINAL_IO_
// Get a character from the USART Receiver buffer
#define _ALTERNATE_GETCHAR_
#pragma used+
char getchar(void)
{
char data;
while (rx_counter==0);
data=rx_buffer[rx_rd_index];
if (++rx_rd_index == RX_BUFFER_SIZE) rx_rd_index=0;
#asm("cli")
--rx_counter;
#asm("sei")
return data;
}
#pragma used-
#endif

// USART Transmitter interrupt service routine
interrupt [USART_TXC] void usart_tx_isr(void)
{
if (tx_counter)
   {
   --tx_counter;
   UDR=tx_buffer[tx_rd_index];
   if (++tx_rd_index == TX_BUFFER_SIZE-1) 
      {
      tx_rd_index=0;       
      RTS=0;
        
      for (k=0;k<= TX_BUFFER_SIZE-1;k++)
         {
         tx_buffer[k]=0;         
         }
      }
   };
}

#ifndef _DEBUG_TERMINAL_IO_
// Write a character to the USART Transmitter buffer
#define _ALTERNATE_PUTCHAR_
#pragma used+
void putchar(char c)
{
while (tx_counter == TX_BUFFER_SIZE);
#asm("cli")
if (tx_counter || ((UCSRA & DATA_REGISTER_EMPTY)==0))
   {
   tx_buffer[tx_wr_index]=c;
   if (++tx_wr_index == TX_BUFFER_SIZE) 
      {
      RTS=0;
      tx_wr_index=0;
      }
   ++tx_counter;
   }
else  UDR=c;
#asm("sei")
}
#pragma used-
#endif

//---------------------------------------------------

// Timer 0 overflow interrupt service routine
interrupt [TIM0_OVF] void timer0_ovf_isr(void)
{
 
TCNT0=-12;
 
   switch (cykl)
      {
      case 0:      //klidovy stav
      amaska=0x01;
      pobit=ukaz_pinu=ukaz_byte=cas=0;
      PL1=1;
      PL2=1;
      suma=0;
      break;

      case 1:
      PL2=0;
      LED_RUN=0;
      if (pol[ukaz_byte]&amaska) 
         {
         PL1=1;
         cykl=2;

         cas = 0;

         while ((pol[ukaz_byte]&amaska)==amaska)
            {
            amaska <<= 1;
				if (amaska==0)
			      {
               amaska=0x01;
		         ukaz_byte++;
					}
            ukaz_pinu += 1;
            
            if ((++suma==2)|(suma==3))
               cas += CAS_PULSU-1;
            else   cas += CAS_PULSU;
            }               
         }
      else   
         {
         cas=0;
         
         PL1=0;
         cykl=10;

         cas = 0;

         while ((pol[ukaz_byte]&amaska)==0)
				{
	        	amaska <<= 1;
				if (amaska==0)
			      {
               amaska=0x01;
		         ukaz_byte += 1;
					}
				ukaz_pinu += 1;

				cas += CAS_PULSU;
				}
			cas =cas-6;
			}
		break;
				
		case 2:
		suma=0;

		PL2=1;
		PL1=0;
		LED_RUN=1;
		cas--;
		cykl=3;
		break;

		case 3:
		cas--;
		if (cas==0)
			{
			PL1=1;
			PL2=0;
			LED_RUN=0;
			cykl=7;
			}
		else	
			{
			if (cas==1)
				PL1=0;
			else	PL1=1;
			}			
		break;

		case 7:
		PL1=0;
		PL2=0;
		LED_RUN=0;
      
      cykl=1;
      
		if (ukaz_pinu >= VYS_POBIT)
			{
			wait=3;
			cykl=8;

			ukaz_byte=0;
			ukaz_pinu=0;
			pobit=0;      
         
			amaska=0x01;   
         
         if (end_cykl)
            {
            end_cykl=0;
            cykl=0;
            }
			}
		break;

		case 8:
		if (--wait==0)
			cykl=1;
		break;

		case 10:
		if (--cas==0)
			cykl=7;
		break;

		default:
		break;
		} 
//        
   tel_cas_0++;
                
   if (wait_eeprom)
      wait_eeprom--;    
   
   if (wait_prenos)
      wait_prenos--;      
                     
   if (wait4)
      wait4--;   
      
    if (wait5)
      wait5--;   
           
   if (b_time_out)
      {
      if (cas_time_out)
         {
         cas_time_out--;
         }
      else
         {
         rx_counter=0;
         rx_wr_index=0;
         b_time_out=0;
         zprava_485=0;
         }
      }   
//      
   if (++cas_100ms==CAS_100MS)
      {
       cas_100ms=0;   
       
       set_100ms=1;   
    /*       
      if (xwait)
         xwait--;  
      
      if (wait2)
         wait2--;   
         
      if (wait3)
         wait3--;  
         
      if (wait_control)
         wait_control--; 
     */
     }        
}

// Timer2 overflow interrupt service routine
interrupt [TIM2_OVF] void timer2_ovf_isr(void)
{
   if (povol_pwm)
     { 
     if (je_mikrofon | pc_hovor | (cykl_tel)) 
        {
        TCNT2=-35;
        }
     else 
      {
      TCNT2=-19;
      }
     
      OCR1AH = 0;
      OCR1AL = sinus[pointr_sinus];
      if (++ pointr_sinus == 255) 
         {
         pointr_sinus=0;
         }    
      }   
}

// External Interrupt 1 service routine
interrupt [EXT_INT1] void ext_int1_isr(void)
{
  tl2= TCNT2; 
  TCNT2=0;   
  
  if (control_odpoved)
   {  
 //pripojeni   bezdrat
vstup=!VSTUP_DATA;  
  
//pripojeni  pres drat    
//vstup=VSTUP_DATA;   

    
 xpol[ukaz_xpol] = tl2;   
           
      if (++ukaz_xpol>=599)
         {
         ukaz_xpol=0;
         }
                
  if (start)           //jiz byl prvni start   
      xzmena=1;     
    
   if (blok_nul)         //prvni znak po trech startbitech
      {                  //nasleduji jedna nebo dve nuly
      blok_nul=0;   
      //xzmena=0; 
  
      if (vstup==1)      //nutna podminka
         {        
         if ((tl2>=PULS_2_MIN)&(tl2<=PULS_2_MAX))    //dve nuly za sebou   //150,200
            { 
            maska <<= 1;           //ulozeni prvni "0"   
            tl2=PRVNI_1;                //prvni "1"    90       //80
            xzmena=0;
            } 
         else
            {  
            //xzmena=0;
            //maska=0x01;
            //ukaz=0;
            //zmena=0;
            //start=0;             
            vstup=0;                //jedna nula
            }               
         }     
      else           //chyba  
         {
         maska=0x01;
         ukaz=xzmena=start=0;   
         }                                                         
      }
      
     if (xzmena)
      {  
      xzmena = 0;

      if ((tl2>=PULS_1_MIN)&(tl2<=PULS_1_MAX))        //jeden znak     //70,100
         {    
			if (vstup==1)
			   pripol[ukaz] |= maska;
			maska <<= 1;
			if (maska==0)
			   {
            maska=0x01;
		      if (ukaz<=POBYTE_VST )
				   ukaz++;
				}
			}

		if ((tl2>=PULS_2_MIN)&(tl2<=PULS_2_MAX))	    //dva znaky        //150,200
		   {
			if (vstup==1)
            pripol[ukaz] |= maska;         
			maska <<= 1;
			if (maska==0)
			   {
            maska=0x01;
				if (ukaz<=POBYTE_VST)
				   ukaz++;
				}                     
//               
         if (vstup==1)
			   pripol[ukaz] |= maska;
			maska <<= 1;
			if (maska==0)
            {
            maska=0x01;
            if (ukaz<=POBYTE_VST)
               ukaz++;
				}
			}
      } 
     
   if (vstup==0)                       //podminka
	   {
		if  ((tl2>=PULS_3_MIN )&(tl2<=PULS_3_MAX))      //start sekvence               //220,250
         {
         maska=0x01;
         ukaz=xzmena=0;                     
         blok_nul=1;                   //test prvni nuly     
		          
        if (start)
            {
            for (i=0;i<=D_ZPRAV;i++)   // zprava pro testovani
		         {
			      pol[i]=~pripol[i];
			      //apol[i]=pripol[i];
               pripol[i]=0; 
			      }  
               
            if (b_cas_blok_zpravy==0)           
               {
               zprava=1;
               }             
             
             /*timsk=TIMSK;  
             gicr = GICR;
             mcucr = MCUCR;   
             mcucsr = MCUCSR;
             gifr=GIFR;*/ 
            NOP;   
            }
        else
            {
            for (i=0; i<= D_ZPRAV;i++)    //po prvne nulovat  
			      pripol[i]=0;            
            }                                        
            
         start=1;      //prisly prvni start bity 
         }
      }   
  }    
      
}

//-------------------------------

void main(void)
{
 
// Input/Output Ports initialization
// Port A initialization
// Func7=Out Func6=Out Func5=Out Func4=In Func3=In Func2=In Func1=In Func0=Out 
// State7=0 State6=0 State5=0 State4=P State3=P State2=P State1=P State0=1 
PORTA=0x1F;
DDRA=0xE1;

// Port B initialization
// Func7=Out Func6=In Func5=In Func4=Out Func3=Out Func2=Out Func1=Out Func0=Out 
// State7=1 State6=P State5=T State4=0 State3=0 State2=0 State1=0 State0=0 

PORTB=0xc0;      //0xc0
DDRB=0xbf;     //0x9F;

//PORTB=0x80;      //0xc0
//DDRB=0xbf;     //0x9F;


// Port C initialization
// Func7=Out Func6=Out Func5=In Func4=In Func3=In Func2=In Func1=Out Func0=Out 
// State7=0 State6=0 State5=T State4=T State3=T State2=T State1=0 State0=0 
PORTC=0x00;
DDRC=0xC3;

// Port D initialization
// Func7=Out Func6=Out Func5=Out Func4=Out Func3=In Func2=Out Func1=In Func0=In 
// State7=0 State6=0 State5=0 State4=0 State3=T State2=0 State1=T State0=T 
PORTD=0x00;
DDRD = 0xa4;  //0x84  

// Timer/Counter 0 initialization
// Clock source: System Clock
// Clock value: 11,719 kHz
// Mode: Normal top=0xFF
// OC0 output: Disconnected
TCCR0=0x05;
TCNT0=0x00;
OCR0=0x00;

// Timer/Counter 1 initialization
// Clock source: System Clock
// Clock value: 12000,000 kHz
// Mode: Ph. correct PWM top=0x00FF
// OC1A output: Non-Inv.
// OC1B output: Discon.
// Noise Canceler: Off
// Input Capture on Falling Edge
// Timer1 Overflow Interrupt: Off
// Input Capture Interrupt: Off
// Compare A Match Interrupt: Off
// Compare B Match Interrupt: Off
TCCR1A=0x81;
TCCR1B=0x01;
TCNT1H=0x00;
TCNT1L=0x00;
ICR1H=0x00;
ICR1L=0x00;
OCR1AH=0x00;
OCR1AL=0x00;
OCR1BH=0x00;
OCR1BL=0x00;

 
// Timer/Counter 2 initialization
// Clock source: System Clock
// Clock value: 11,719 kHz
// Mode: Normal top=0xFF
// OC2 output: Disconnected
ASSR=0x00;
TCCR2=0x07;
TCNT2=0x00;
OCR2=0x00;

// External Interrupt(s) initialization
// INT0: Off
// INT1: On
// INT1 Mode: Any change
// INT2: Off
GICR|=0x80;
MCUCR=0x04;
MCUCSR=0x00;
GIFR=0x80;

// Timer(s)/Counter(s) Interrupt(s) initialization
TIMSK=0x01; //0x41

// USART initialization
// Communication Parameters: 8 Data, 1 Stop, No Parity
// USART Receiver: On
// USART Transmitter: On
// USART Mode: Asynchronous
// USART Baud Rate: 19200
 
UCSRA=0x00;
UCSRB=0xD8;
UCSRC=0x86;
UBRRH=0x00;
UBRRL=0x26;

// Analog Comparator initialization
// Analog Comparator: Off
// Analog Comparator Input Capture by Timer/Counter 1: Off
ACSR=0x80;
SFIOR=0x00;

MOTOROLA=PTT=EXT_MOD=RELE_PC=RELE_MIC=RELE_TEL=0;
RELE_AUDIO=POWER_DRAT=MODULACE_DRAT=PIN_GSM=0;
//BLOK_ZNELKY=SET;
ZNELKA=RESET;
POVOL_TEL=1;

RTS=0;
byl_znak=0;


// Global enable interrupts
#asm("sei")

  // stav[povoleni] = 0x01;      //mazani
  // serial(VSEM,0,0);   

  // stav[povoleni] = 0;     
                             
  
  // I2C Bus initialization    
i2c_init();

// PCF8583 Real Time Clock initialization
rtc_init(0,0);

    // nastaven� watchdogu
  WDTCR=0x0d;    
  b_restart = 1;      
  
    
  cteni_zaznamu =  E_cteni_zaznamu;
  WAIT_EEPROM;
  NOP;
  
  zapis_zaznamu = E_zapis_zaznamu;
  WAIT_EEPROM;   
 
  cas_telefon = 20;  
      
  NOP;    
                                                 
while (1)
      {        
      #asm("wdr");    
          
      if (JP3_MOTOROLA)         //zapnuti vysilacky
		   {
         if (set_motorola)
            ;
         else
            {   
		      set_motorola=1;
		      MOTOROLA=1;
            }
		   }
	   else
		   {
		   if (set_motorola)
			   {
			   set_motorola=0;
			   MOTOROLA=0;
            }
			}           
//
// telefon  
    if (cas_telefon)
      {
      tel_zm_vst  = ZADOST_TEL;     //funguje po startu 
      
       if (tel_zm_vst != tel_zm_por)
         {
         tel_zm_por =  tel_zm_vst;
         }      
      }

   if (((stav[status1] & (je_audio|je_pc|je_pult)) == 0) & (cas_telefon==0))
      {
      tel_zm_vst  = ZADOST_TEL;                    
      tel_zm1     = (tel_zm_vst^tel_zm_star) & tel_zm_vst;
      tel_zm0     = (tel_zm_vst^tel_zm_star) & tel_zm_star;
      tel_zm_star = tel_zm_vst;
//
      if (tel_zm1)              
         { 
         tel_zm1 = 0;
         
         pocet_prvni_puls++;
                  
         /*
         xpol[ukaz_xpol] = tel_cas_0;         
         if (++ukaz_xpol == 100)
            {
            ukaz_xpol =0;
            }             
         */
         
         pol_tel[ukaz_tel]++;                 
         NOP;     
                                                       
         if ((tel_cas_0 >20)&(tel_cas_0 <=33))      //jina cislice
               {               
               ukaz_tel++;  
               byl_znak=1;                         
               NOP;
               } 
                                            
         if ((tel_cas_0 >100)&(pocet_prvni_puls>=2))  //konec prenosu
            { 
            pocet_prvni_puls=0;       
                                     
            pol_tel[0] -= 2; 
            
            NOP;   
                  
            if (pol_tel[0] >= 17)
               {               
               in[0].vst = 0xff;
               in[1].vst = 0xff;
               NOP;                              
               }
                  
            else
               {
               for (j=0;j<=ukaz_tel;j++)
                  {                      
                  switch (pol_tel[j])
                     {
                     case 1: 
                     in[0].vst |= 0x01;                   
                     break;    
                  
                     case 2: 
                     in[0].vst |= 0x02;
                     break; 
                   
                     case 3: 
                     in[0].vst |= 0x04;            
                     break;  
                  
                     case 4: 
                     in[0].vst |= 0x08;
                     break;                             
                  
                     case 5: 
                     in[0].vst |= 0x10;             
                     break;     
                  
                     case 6: 
                     in[0].vst |= 0x20;
                     break;           
                  
                     case 7: 
                     in[0].vst |= 0x40;
                     break;           
                  
                     case 8: 
                     in[0].vst |= 0x80;                  
                     break;  
                  
                     case 9:                   
                     in[1].vst |= 0x01;
                     break;
                   
                     case 10:                   
                     in[1].vst |= 0x02;
                     break;           
                  
                     case 11:                   
                     in[1].vst |= 0x04;
                     break;           
                  
                     case 12:                   
                     in[1].vst |= 0x08;
                     break;           
                  
                     case 13:                   
                     in[1].vst |= 0x10;
                     break;           
                  
                     case 14:                   
                     in[1].vst |= 0x20;
                     break;           
                  
                     case 15:                   
                     in[1].vst |= 0x40;
                     break;           
                  
                     case 16:                   
                     in[1].vst |= 0x80;
                     break;
                  
                     default:
                     break;
                     }
                  } 
               }
//           
            bbb =  in[0].vst;
            ccc =  in[1].vst;
            
            NOP;
            
            if (tel_cas_0 >200)
                {  
                control=0;
                
                //nove
                 for (j=0;j<=ukaz_tel;j++)
                  {
                  pol_tel[j]=0;                  
                  }            
                                               
               cykl_tel=0;                    
               tel_zm1 = tel_zm_vst = tel_zm_star = tel_zm0 = 0;
               pocet_prvni_puls = 0;
               ukaz_tel = 0;
               stav[status1] &= ~je_telefon;    
                }
                            
            else 
             { 
               byl_znak=0;
                          
               POVOL_TEL=0;
               stav[status1] |= je_telefon;
                    
               pocet_prvni_puls = 0;
            
               uschov_serial=VSEM;    
               zmena=1;       
                  
               cykl_tel = 2;
            
               cas_nouze=CAS_NOUZE;                  
               cas_telefonu = 20;// *100[ms]
            
               if (control >= 6)
                  {
                  control=1;
                  PTT=0;
                  }                        
               }  
            }                                                       
         }
       
      if (tel_zm0)
         { 
         tel_zm0 = 0;
         tel_cas_0=0;      
         //ukaz_tel++;   
         }                                     
      }
//
   if  (cykl_tel==2)
      {
      if (cas_telefonu==0)              //polozeni sluchatka
         {
         if ((ZADOST_TEL==1)|(cas_nouze==0))
            {
            stav[status1] &= ~je_telefon;
            
            if (JE_PC|JE_PULT|JE_AUDIO)
               {
               NOP;
               }
            else
               {
               inpol[p_DATA3] = mazani;
                   
               cykl_tel=0;   
               
               //byl_znak=0;
               zmena=1;
               }
            }
         }    
      }
//
       if (zprava_485)
         {
         zprava_485=0;
      
         ksuma=0;
         for (j=0;j<=RX_BUFFER_SIZE-1;j++)
            {
            ksuma += rx_buffer[j];
            inpol[j] =  rx_buffer[j];
            rx_buffer[j]=0;
            }
            
         rx_wr_index=zprava_485 = 0;
                      
         if (ksuma==0)
            {
            b_time_out=0;
            
            if ((inpol[p_STX]==STX)&&(inpol[p_ETX]==ETX)&&(inpol[p_ADRESA_KAM]==Z_DESKA))
               {
               switch (inpol[p_ADRESA_ODKUD])
                  {
                  case PULT: 
                  if (servis==0)
                     {                  
                     if (MAZANI)
						      {
                        konec_vysilani=1;
                        RELE_PC=RELE_AUDIO=0;
                        POVOL_TEL=1;
                     
                        for (j=0;j<=1;j++)
                           {
                           in[j].stav=0;
						         in[j].star=0;
                           }
                         
                        stav[povoleni]=0x01;   
						      stav[status1]=0;
                        stav[status2]=0;
                        stav[rezerva]=0; 
                        stav[stav_servis]=0;
						   
                        uschov_serial=VSEM;   
						      zmena=1;
                        }
                     
                     if (stav[status1]&je_pc)
                        {
                        NOP;  
                        }  
                     else
                        {           
                        in[0].vst = inpol[p_DATA1];
                        in[1].vst = inpol[p_DATA2];
                       
                        if  (stav[status1]&(je_pult|je_test)) 
                           {          
                           NOP;
                           }
                        else
                           {   
                           stav[status1] |= je_pult; 
                           }      
                        zmena=1;	
						      uschov_serial=VSEM;
                        }                        
                     }
                  break;
                     
                  case PC: 
                   if (servis==0)
                     {                        
                     if (inpol[p_DATA7] == PC_HOVOR_START)
                        {
                        pc_hovor=1;   
//                        
                        for (j=0;j<=p_KSUMA;j++)
		                     tx_buffer[j]=0;
                                                  
                        tx_buffer[p_STX]=STX;
	                     tx_buffer[p_ADRESA_KAM]=PC;
	                     tx_buffer[p_ADRESA_ODKUD]=Z_DESKA;
	                     tx_buffer[p_DATA7] = PC_HOVOR_START;                          
	                     tx_buffer[p_ETX]=ETX;   
	                     serial2();   
                        break;                     
                        }   
//
                     if (inpol[p_DATA7] == PC_HOVOR_STOP)
                        {
                        pc_hovor=0;   
//
                       for (j=0;j<=p_KSUMA;j++)
		                     tx_buffer[j]=0;
                                                  
                        tx_buffer[p_STX]=STX;
	                     tx_buffer[p_ADRESA_KAM]=PC;
	                     tx_buffer[p_ADRESA_ODKUD]=Z_DESKA;	                                                   
                        tx_buffer[p_DATA7] = PC_HOVOR_STOP;                           
	                     tx_buffer[p_ETX]=ETX;                          
                        serial2();	 
                        break;                    
                        }                     
//                     
                     if (inpol[p_DATA7]==ROZVADEC_R3)
                        {                                                                   
                        for (j=0;j<=p_KSUMA;j++)
		                        tx_buffer[j]=0;
                                                  
                        tx_buffer[p_STX]=STX;
	                     tx_buffer[p_ADRESA_KAM]=PC;
	                     tx_buffer[p_ADRESA_ODKUD]=Z_DESKA;
	                                                   
                        #ifdef DEF_ROZVADEC_R3
                           tx_buffer[p_DATA7]=ROZVADEC_R3;
                        #else
                        tx_buffer[p_DATA7]=0;
                        #endif
                        
	                     tx_buffer[p_ETX]=ETX;   
                        
                        NOP;
   
	                     serial2();                  
                        }
                     else
                        {                                         
                        if  (stav[status1]&(je_audio|je_pult))     //je-li pult nebo audio
                           {
                           NOP; 
                           break;
                           }
                        else
                           {   
                           if (MAZANI)
                              {
                              konec_vysilani=1;
                              RELE_PC=0;
                              }
                    
                           if  (stav[status1]&je_pult)  
                              {
                              uschov_serial=PC;
                              }   
                           else
                              {
                              in[0].vst = inpol[p_DATA1];        //zprava pro PC
                              in[1].vst = inpol[p_DATA2];
                      
                              if (stav[status1]&(je_pc|je_test))   
                                 {
                                 NOP;
                                 }
                              else
                                 {
                                 if ((inpol[p_DATA4]&test)!=test)
                                    {
                                    stav[status1] |= je_pc;
                                    }
                                 }
                              uschov_serial=VSEM;
                              }   
                           }
                        zmena=1;
                        }      
                     }
                  break;
                     
                  case AUDIO: 
                   if (servis==0)
                     {                     
                     if ((inpol[p_DATA5]&kontrola)==kontrola)
                        {
                        test_kontrola=0;
                        }
                  
                     if (JSVV)
                        {                     
                        POVOL_TEL=1;
                        RELE_PC=0;   
                        
                        
                        //aaaaaaaaaaaaaaaaaaaaaaaaaaa
                        
                        if (stav[status1] & (je_pc|je_pult) != 0)
                           {                          
                           control_audio=1;                          
                           }                         
                        else
                           {                      
                           je_ext = je_znelka = je_mikrofon=0;   
                           in[0].vst=0xff;  
                           in[1].vst=0xff;
                                        
                           control=1;
                           //stav[status1] &= ~(je_pc|je_pult|je_telefon);
                           stav[status1] = je_audio | ready;  
                           uschov_serial=VSEM;  
                           }
                        }
                     else
                        {
                        if (KONEC_SIRENY)
                           {
                           stav[status1] &= ~je_audio;
                           in[0].vst=0;
                           in[1].vst=0;
                           stav[povoleni] = mazani;
                           uschov_serial=VSEM;
                           RELE_AUDIO=0;  
                           }  
                        }     
                     NOP;   
                     zmena=1;   
                     NOP;
                     }	
                  break;  
                     
                  case ACH:
                  if (servis==0)
                     {    
                     if  ((stav[status1]&(je_pult|je_test|je_pc|je_audio))==0)                    
                        {                   
                        if ((inpol[p_DATA5]&start_ach)==start_ach)
                           {              
                           control_ach=1;                       
                           }                     
                  
                        if (MAZANI)
                           {         
                           stav[status1] = 0;
                           stav[status2] = 0;
                           inpol[p_DATA3]=mazani;
                           zmena=1;                                                       
                           }                             
                        }                     
                     }
                  break;      
                  
                  case SERVIS:   
                  if  ((stav[status1]&(je_pult|je_test|je_pc|je_audio))==0)
                     {                   
                     if ((inpol[aservis]&start_servis)==start_servis)
                        {              
                        servis=1;                       
                        }                     
                  
                     if (MAZANI)
                        {   
                        wait3=10;                       
                        servis=7;
                        }                             
                     }
                   break;  
                   
                   default:
                   break;                   
               } 
               
            if (inpol[p_ADRESA_ODKUD]==PC)
            {
            if ((inpol[p_DATA4]&test)==test)
               {                     
               zmena=0;  
               stav[povoleni] &=~ mazani;       
                    
               serial(uschov_serial,in[0].stav,in[1].stav);
               stav[status1] &= ~je_test;
               }
               
          
            //if (stav[status1] &= je_pult|je_audio)
             //  {
               // zmena=0;
               //}
              
            }      
            } 

            if ((inpol[p_STX]==STX)&&(inpol[p_ETX]==ETX)&&(inpol[p_ADRESA_KAM]== DESKA_ODPOVED))
               {
               switch  (inpol[p_DATA1])
                  {
                  case CTENI_EEPROM:
                      
                  for (j=0;j<=19;j++)
                     tx_buffer[j]=0;
                  
                  zapis_zaznamu = E_zapis_zaznamu;
                  WAIT_EEPROM;
                  cteni_zaznamu = E_cteni_zaznamu;
                  WAIT_EEPROM;
                  
                  if (cteni_zaznamu != zapis_zaznamu)
                     {                         
                     tx_buffer[p_DATA1]  = CTENI_EEPROM;
                     tx_buffer[p_DATA2] = zaznam[cteni_zaznamu].cislo_hlasice;
                     WAIT_EEPROM;                                       
                     tx_buffer[p_DATA3] = zaznam[cteni_zaznamu].hod;
                     WAIT_EEPROM;
                     tx_buffer[p_DATA4]=zaznam[cteni_zaznamu].min;
                     WAIT_EEPROM;
                     tx_buffer[p_DATA5]= zaznam[cteni_zaznamu].sec;
                     WAIT_EEPROM;
                     tx_buffer[p_DATA6]=zaznam[cteni_zaznamu].den;
                     WAIT_EEPROM;
                     tx_buffer[p_DATA7]=zaznam[cteni_zaznamu].mesic;
                     WAIT_EEPROM;
                     tx_buffer[p_DATA8]=zaznam[cteni_zaznamu].rok;
                     WAIT_EEPROM;
                     tx_buffer[p_DATA9]=zaznam[cteni_zaznamu].byte1;
                     WAIT_EEPROM;
                     tx_buffer[p_DATA10]=zaznam[cteni_zaznamu].byte2;
                     WAIT_EEPROM;   
               
                     if (++cteni_zaznamu == MAX_ZAZNAM)
                        {
                        cteni_zaznamu=0;
                        tx_buffer[p_DATA1]= KONEC;
                        }
                        
                     E_cteni_zaznamu = cteni_zaznamu;
                     WAIT_EEPROM;
                     
                     serial1(CTENI_EEPROM);   
                     } 
                  else
                     {
                     serial1(NENI_EEPROM);                      
                     }                                                          
                  break;
                     
                  case NASTAVENI_CASU:   
                                      
                  hour  = inpol[p_DATA3];
                  min   = inpol[p_DATA4];
                  sec   = inpol[p_DATA5];
                  date  = inpol[p_DATA6];
                  month = inpol[p_DATA7];
                  year_uch  = inpol[p_DATA8]; 
                    
                  year = year_uch+2000;       
                  
                  NOP;NOP;NOP;
                         
                  #asm("cli");                                                
                  rtc_set_time(0,hour,min,sec,0);                      
                  #asm("sei"); 
                  
                  WAIT_EEPROM;
                                    
                  #asm("cli");                                 
                  rtc_set_date(0,date,month,year);    
                  #asm("sei");        
                                     
                  NOP;NOP;NOP;
                                                                                             
                  #asm("cli");                                        
                  rtc_get_time(0,&ho,&mi,&se,&hs);
                  #asm("sei");  
                   
                  WAIT_EEPROM;
                  
                  #asm("cli");                                    
                  rtc_get_date(0,&da,&mo,&ye);  
                  #asm("sei");   
                                                                                                             
                  tx_buffer[p_DATA3] =  ho;
                  tx_buffer[p_DATA4] =  mi;
                  tx_buffer[p_DATA5] =  se;
                  tx_buffer[p_DATA6] =  da;
                  tx_buffer[p_DATA7] =  mo;
                  tx_buffer[p_DATA8] = ye-2000;                     
                  //                     
                  serial1(PRIJATA_ZPRAVA_CAS); //obdrzeni zpravy   
                  NOP;               
                  break;   
                  
                  case CTENI_CASU:     
                  #asm("cli");                                                                                     
                  rtc_get_time(0,&ho,&mi,&se,&hs);
                  NOP;                                    
                  rtc_get_date(0,&da,&mo,&ye);  
                  #asm("sei");                                         
                  NOP;   

                  tx_buffer[p_DATA3] =  ho;
                  tx_buffer[p_DATA4] =  mi;
                  tx_buffer[p_DATA5] =  se;
                  tx_buffer[p_DATA6] =  da;
                  tx_buffer[p_DATA7] =  mo;
                  tx_buffer[p_DATA8] =  (ye-2000)&0xff; 
                  //                     
                  serial1(CTENI_CASU); //obdrzeni zpravy   
                  NOP;               
                  
                  break;
                  

                  case PC_INFO:
                 //vytvorit vysilani
 
                  pomo_pol[0]=KOD_APLIKACE_ODPOVED; 
		            pomo_pol[1]=inpol[p_DATA2];     //cislo hlasice   
                  //pomo_pol[2] = 0x01;     //set_rele       0x01     
                  
                  pomo_pol[2] = inpol[p_DATA10]&0x04;   //doplnek rele
                  pomo_pol[2] >>= 2;
                  pomo_pol[2] &= 0x01;
                  
                  pomo_pol[3]=ODPOVED_HLASIC;
                  //pomo_pol[3]=0; 
                  pomo_pol[4] = 0;
                  pomo_pol[5] = 0;
                  
                  control_odpoved=1;          
                  
                  for (j=0;j<=20;j++)
                     tx_buffer[j]=0;     
                     
                  posl_cislo_hlasice =  inpol[p_DATA2];  
                  
                  tx_buffer[p_DATA2] = inpol[p_DATA2];  //cislo hlasice                                                                    
                  
                  serial1(PRIJATA_ZPRAVA_INFO); //obdrzeni zpravy
                     
                  //zmena registru !!!!                                       
                  break;                
                  
                  case SET_GSM:            
                  //                 
                  gsm_ram.cislo_hlasice = inpol[p_DATA2];                     
                  gsm_ram.typ  =  inpol[p_DATA3];                               
                  gsm_ram.stav =  inpol[p_DATA4]; 
                  
                  if (gsm_ram.typ == 0x08)
                      gsm_ram.stav = 1;   
                      
                   if (gsm_ram.typ == 0x10)
                      gsm_ram.stav = 1;
                  
                  aa =  gsm_ram.cislo_hlasice;
                  bb =  gsm_ram.typ;
                  cc =  gsm_ram.stav;
                  
                  NOP;
                  
                  
                   gsm_ram.stav &= ~ODESLANA_SMS;                                                           
                  //                                                 
                  for (j=0;j<=20;j++)
                     tx_buffer[j]=0;   
                  //   
                  tx_buffer[p_DATA2] = gsm_ram.cislo_hlasice;
                  WAIT_EEPROM; 
                  tx_buffer[p_DATA3] = gsm_ram.typ;
                  WAIT_EEPROM;                     
                  tx_buffer[p_DATA4] = gsm_ram.stav;                   
                  //                                
                  serial1(SET_GSM);           
                  
                  gsm_eeprom.cislo_hlasice = gsm_ram.cislo_hlasice;    
                  WAIT_EEPROM;
                  gsm_eeprom.typ  = gsm_ram.typ;
                  WAIT_EEPROM;               
                  gsm_eeprom.stav =  gsm_ram.stav;    
                  WAIT_EEPROM;                                 
                  //                  
                  break;
                                              
                  case GET_GSM:                   
                  for (j=0;j<=20;j++)
                     tx_buffer[j]=0;   
                                      
                  tx_buffer[p_DATA2] = gsm_eeprom.cislo_hlasice;
                  WAIT_EEPROM; 
                  tx_buffer[p_DATA3] = gsm_eeprom.typ;
                  WAIT_EEPROM;                     
                  tx_buffer[p_DATA4] = gsm_eeprom.stav;                   
                  WAIT_EEPROM;                           
                  //
                  serial1(GET_GSM);                                         
                  break;     
//                                  
                  default:
                  break;
                  }                      
               }   
           }  
         }  
         
 //konec 485                  
    
         if (zmena)
            {
            zmena=0; 
              
       	   if (MAZANI)
                	{ 
                   if ((control)|(servis)|(control_odpoved)|(control_ach))
                     {                   
                     control=6;
                     konec_vysilani=1;    
                     servis=control_odpoved=control_ach=0;
                     }
                                          
                  je_znelka=je_ext=je_mikrofon=0;
                  je_drat=je_mz=0;
                  set_test=0;
                  for (j=0;j<=1;j++)
                     {
                     in[j].vst=0;
                     in[j].stav=0;
                     }
                  
                  inpol[p_DATA3] &= ~mazani;
                  stav[povoleni]=0x01;
                  
                  POVOL_TEL=1;         
                  RELE_PC=0;
                  RELE_AUDIO=0;
                  RELE_TEL=0;   
                  RELE_MIC=0;
                  MZ_POWER=RESET;
                  MODULACE_DRAT=0;
                  }            
            
            if ((inpol[p_DATA4]&test_rezim)==test_rezim)
               {
               set_test=1;
               in[0].vst = 0xff;
               in[1].vst = 0xff;
               stav[status1] |= test_rezim; 
               }      
       
 	         if (GENERAL)
               {
               in[0].vst =0xff;
               in[1].vst =0xff;
               in[0].stav=in[1].stav=0; 
               inpol[p_DATA3] &= ~general;
		         }     
                           
            for (j=0;j<=1;j++)
                  {
                  in[j].zm1 =(in[j].vst^in[j].star)&in[j].vst;
                  //in[j].zm0 =(in[j].vst^in[j].star)&in[j].star;
                  in[j].zm0 = in[j].zm1&in[j].stav;
                  in[j].stav=(in[j].stav|in[j].zm1)&(~in[j].zm0);
                  in[j].star=in[j].vst=0;
	        		   in[j].star=0;
                  in[j].vst=0;
                  } 
          
             if ((in[0].stav|in[1].stav)==0)
                {
                stav[status1] &= ~(je_pc|je_pult|je_audio|je_telefon); 
                
                uschov_serial=VSEM;
                
                if (stav[status1]&je_test)
                  stav[status1] &= ~je_test;
                else stav[povoleni] |= mazani;   
                }
                   
             if (stav[status1] & (je_pc|je_audio|je_telefon|je_pult|test_rezim)) 
               {
                if ((control==0))
                  {
                  //if (control >= 6)
                    // PTT=0;
                  control=1;   
                  }
               }
                  
/*
              if ((control==1)|(control==6))
               {   
               pomo_pol[0]=KOD_APLIKACE;	// kod aplikace
		         pomo_pol[1]=in[0].stav;
		         pomo_pol[2]=in[1].stav;
               pomo_pol[3] = 0;
               pomo_pol[4] = 0;
               pomo_pol[5] = 0;
                              
               if (set_test)
                   pomo_pol[3] |= 1;

               for (j=0;j<= 9;j++)
                  pol[j]=0;     
                  
               //if (pocet_1(pomo_pol,POBIT))
                 //  pomo_pol[3] |= 0x02;   
                 
                  
               prev_t(pol,pomo_pol,POBIT);  
               NOP;
               }	 
*/
//       
              if (set_test==0)
               { 
               if (JP1_MZ)
			         { 	
			         if  ((in[0].stav&0x01)==0x01)	//Tla��tko 1
				         {
				         if (je_mz)
					         ;
				         else  je_mz=1;
				         }
			         }
            
                  if (JP2_DRAT) 
			            { 	
			            if  ((in[1].stav&0x02)==0x02)	//Tla��tko 10
				            {
				            if (je_drat)
					            ;
				            else
					            {
					            je_drat=1;
					            POWER_DRAT=1;
					            }
				            }
			            }                
             }
// 
            if (JE_EXT_MOD)    //---ext_modulace----------	
               {
               if (je_ext)
                   {
                  je_ext=0;
                   stav[povoleni]=0;
                  //  ????? casovac_modulace=CAS_MODULACE;
                  EXT_MOD=0;
                  }
               else
                  {
                   if (control==0)
                     control=1;   
                  je_ext=1;
                  stav[povoleni]=0x08;
                  if (control==5)
                    {
                    EXT_MOD=1;
                    }
                  }
               }
            else  EXT_MOD=0;
//
         	if (JE_ZNELKA)       //----znelka-------
               {  
                if (control==0)
                  control=1;
                
               inpol[p_DATA3] &= ~znelka;  	 
               je_znelka=1;
				   if (control==5)
                  { 
				     // ZNELKA=SET;
                  if (sm_gong==0)
                     sm_gong=1;
                  }
               stav[povoleni]=0x10;  
				   }
//
            if (MIKROFON)
                  {
                  if (je_mikrofon)
                     {
                     je_mikrofon=je_ext=0;
                     RELE_MIC=0;
                     stav[povoleni]=0;
                     }
                  else
                     {
                     if (control==0) 
                        control=1;
                        
                     je_mikrofon=1;
                     if (control==5)
                        {
                        RELE_MIC=1;
                        }
                     stav[povoleni]=0x04;
                     }
                  }
            else  RELE_MIC=0;
                                
             serial(uschov_serial,in[0].stav,in[1].stav); 
             
             stav[status1] &= ~je_test;
              
             if (konec_vysilani)
               {
               konec_vysilani=0;
               stav[povoleni]=0;
               stav[status1]=0;
               stav[status2]=0;
               }     
			   }      
                      
         switch (control)
            {
            case 0:
            break;
            
            case 1:
            MOTOROLA=1;
            wait_control=CAS_MOTOROLA;    //3,5 [sec]
            control=2;  
                     
            doba_provozu=36000;  //60 [min]
            b_doba_provozu=1;
            break;
            
            case 2:
            if (wait_control==0)
               { 
               pomo_pol[0]=KOD_APLIKACE;	// kod aplikace
		         pomo_pol[1]=in[0].stav;
		         pomo_pol[2]=in[1].stav;
               pomo_pol[3] = 0;
               pomo_pol[4] = 0;
               pomo_pol[5] = 0;
                              
               if (set_test)
                   pomo_pol[3] |= 1;

               for (j=0;j<= 9;j++)
                  pol[j]=0;     
                                    
               prev_t(pol,pomo_pol,POBIT);  
               NOP;
               
               PTT=1;
               control=3;
               //wait_control=5;    //500  [msec]    
               wait_control=10;     //4000  [msec]             
               }
            break;
            
            case 3:
            if (wait_control==0)
               {  
               cykl=1;  
               control=4;
               
               b_doba_vysilani=1;
               doba_vysilani= DOBA_VYSILANI;         // pro Zrx 85;   //  *100 [ms]    
               periferie=0;
               }
            break;
            
            case 4:
             if (periferie)
               {
               periferie=0;
               stav[status2] = vysilani;
               serial(VSEM,in[0].stav,in[1].stav);
                           
               if (stav[status1] & je_pc) 
                  RELE_PC=1;
                  
               if (stav[status1] & je_telefon) 
                  {
                  RELE_TEL=1;
                  }
                  
               if (stav[status1] & je_audio) 
                  {
                  RELE_AUDIO=1;
                  }
                       
                  
               if ((stav[status1] & je_telefon) == 0)
                  {
                  if (je_mikrofon)
                     {
                     RELE_MIC=1;
                     }
                  }   
                       
               if (je_znelka)
                  {
                   ZNELKA=RESET;  
              
                 //if  (stav[status1]&je_test)
                   if (set_test)
                     {
                     sm_gongtest=1;  
                     sm_gong=1;
                     }  
                  else
                     { 
                     if (sm_gong==0)
                        sm_gong=1;                     
                     }                     
                  
                  if (sm_gong==0)
                        sm_gong=1;                     
                  }
                  
               if ((stav[status1] & je_telefon) == 0)
                  {
                  if (je_ext)
                     {
                     EXT_MOD=1;
                     }
                  }         
                  
               if (je_drat)
                  MODULACE_DRAT=1;
                  
                if (je_mz)
                  MZ_POWER=SET;  
                      
               control=5;
               if (je_audio)
                  cas_kontroly=300;   
            
                         
            ASSR=0x00;
            TCCR2=0x06;
            TCNT2=0x00;
            OCR2=0x00;

            TIMSK=0x41;  
            
            GICR = 0;
            MCUCR=0;
            MCUCSR=0;
            GIFR=0;
            }                
            break;
             
            case 5:
            if (!povol_pwm)
               {             
               if (b_doba_vysilani==0)
                  { 
                  povol_pwm = 1;                          
                  }
               }               
            break;
            
            case 6:             //mazani relace
            b_doba_provozu=0; 
            servis=0;
            
            EXT_MOD=RELE_PC=RELE_MIC=RELE_TEL=0;
            RELE_AUDIO=MODULACE_DRAT=0;     
            
            servis = control_ach = 0;
            //ZNELKA=RESET;
              if (sm_gong)
               {   
               sm_gong=5;           
               wait2=2;
               }         
               
            if (sm_gongtest)
               {   
               sm_gongtest=5;           
               wait2=2;
               }      
                        
            POVOL_TEL=1;   
            //
            //nove
              for (j=0;j<=ukaz_tel;j++)
                  {
                  pol_tel[j]=0;                  
                  }            
                                               
               cykl_tel=0;                    
               tel_zm1 = tel_zm_vst = tel_zm_star = tel_zm0 = 0;
               pocet_prvni_puls = 0;
               ukaz_tel = 0;
               stav[status1] &= ~je_telefon;               
            //
            wait_control=20;    // 2 [sec
            control=7;  
            //sm_gong=sm_gongtest=0;  
            
            //serial(VSEM,0,0);                      
            break;
            
            case 7:
            if (wait_control==0)
               {  
//               
               pomo_pol[0]=KOD_APLIKACE;	// kod aplikace
		         pomo_pol[1]=0;
		         pomo_pol[2]=0;
               pomo_pol[3] = 0;
               pomo_pol[4] = 0;
               pomo_pol[5] = 0;
                                           
               for (j=0;j<= 9;j++)
                  pol[j]=0;     
                                    
               prev_t(pol,pomo_pol,POBIT);  
               NOP;                                            
//               
               povol_pwm = 0;   
               //wait_control=30;
               b_doba_vysilani=1;
               doba_vysilani= DOBA_VYSILANI; //6 [s]
               cykl=1;
               control=8;                    
               }
            break;
            
            case 8:
             if (periferie)
               {
               periferie=0;
               PTT=0;
               wait_control=30;
               control=9;
               }
            break;
            
            case 9:
             if (wait_control==0)
               { 
               b_doba_provozu=0;
               
               MOTOROLA=0;
               MZ_POWER=RESET;
               POWER_DRAT=0;
               cykl=0;
               periferie=0;
               stav[povoleni]=0;
               stav[status1]=0;
               stav[status2]=0;
               stav[rezerva]=0;     
                              
               //vratit timer2               
               TCCR2=0x07;
               TCNT2=0x00;
               OCR2=0x00;           
               
               //vratit preruseni               
               TIMSK=0x01;   
               
               //   vr�tit ext.int
               GICR|=0x80;
               MCUCR=0x04;
               MCUCSR=0x00;
               GIFR=0x80;  
               
               NOP;
               
               while (control);  //WD
               //control=0;    
               }
            break;
            
            default:
            break;
            }    
 
		switch (sm_gong)
			{
			case 0:
			break;

			case 1:
			wait2=10;    //20
         sm_gong=3;
			break;

			case 3:
			if (wait2==0)
				{
				ZNELKA=SET;
				wait4= 100;
				sm_gong=4;
				}
			break;

			case 4:
			if (wait4==0)
				{         
            ZNELKA=RESET;
				wait2=DOBA_ZNELKY;
            sm_gong=5;
				}
			break;

			case 5:
			if (wait2==0)
				{
				ZNELKA=SET;
				wait4=100;
				sm_gong=6;
				}
			break;

			case 6:
			if (wait4==0)
				{
				wait2=3;
				ZNELKA=RESET;
				sm_gong=8;
            stav[povoleni]=0;
				}
			break;

			case 8:
			if (wait2==0)
				{
				sm_gong=0;
				je_znelka=0; 
            stav[povoleni] &= (~0x1c);
			   serial(PULT,in[0].stav,in[1].stav);	   
            NOP;           
         	}
		   }    
                    
         switch (sm_gongtest)
            {
            case 0:
            break;
            
            case 1:
			   wait2=10;    //20
            sm_gongtest=3;
			   break;

			   case 3:
			   if (wait2==0)
				   {
				   ZNELKA=SET;
				   wait4=100;
				   sm_gongtest=4;
				   }
			   break;

			   case 4:
			   if (wait4==0)
				   {
				   ZNELKA=RESET;
				   wait2=DOBA_ZNELKY;
               sm_gongtest=5;
				   }
			   break;

			   case 5:
			   if (wait2==0)
				   {
				   ZNELKA=SET;
				   wait4=100;
				   sm_gongtest=6;
				   }
			   break;

			   case 6:
			   if (wait4==0)
				   {
				   wait2=30;
				   ZNELKA=RESET;
				   sm_gongtest=8;
               stav[povoleni]=0;
				   }
			   break;
       
			   case 8:
			   if (wait2==0)
				   {
				   //sm_gongtest=9;
               //wait2=55;
               sm_gongtest=0;
               //cykl=1;
         	   }
            
            default:
            break;
            }               
//                       
        if ((stav[status1] & (je_audio|je_pc|je_pult))==0)
         {
         switch (servis)
            {
            case 0:    
            break;            
            
            case 1:
            MOTOROLA=1; 
            wait3=CAS_MOTOROLA;      //3 [sec]
            servis=2;               
            break;    
                                 
            case 2:
            if (wait3==0)
               {
               pomo_pol[0]=KOD_APLIKACE;	// kod aplikace
		         pomo_pol[1]=0xff;
		         pomo_pol[2]=0xff;
               pomo_pol[3]=0x01;    //test rezim
               pomo_pol[4] = 0;
               pomo_pol[5] = 0;
                              
               for (j=0;j<= 9;j++)
                  pol[j]=0;
       
               prev_t(pol,pomo_pol,POBIT);    
                 
               PTT=1;
               wait3=10;
               servis=3;
               }
            break;      
            
            case 3:
            if (wait3==0)
               {
               cykl=1;                 
               b_doba_vysilani=1;
               doba_vysilani= DOBA_VYSILANI;   // *100 [ms]                   
               servis=4;
               }
            break;
                    
            case 4:
            if (b_doba_vysilani==0)
               {  
//               
               ASSR=0x00;        // povoleni PWM
               TCCR2=0x06;
               TCNT2=0x00;
               OCR2=0x00;
                  
               TIMSK=0x41;  
            
               GICR = 0;
               MCUCR=0;
               MCUCSR=0;
               GIFR=0;                              
//                
               povol_pwm=1; 
               
               wait3=10;
               servis=5;
               }
            break;
                                                
            case 5:
            if (wait3==0)
               {     
               RELE_MIC=1;
               wait3=10;
               servis=6;
               }
            break;
                        
            case 6:                 
             if (wait3==0)
               { 
               stav[status2] = vysilani;
               serial(SERVIS,0,0);
               wait3=600;             //maximalni cas  1 [min]
               servis=7;
               }            
            break;
            
            case 7:
             if (wait3==0)
               {                
               inpol[p_DATA3]=mazani;
               zmena=1; 
               wait3=130;
               servis=8;
               }            
            break;   
                       
            case 8:
             if (wait3==0)
               {             
               servis=0;
               }
            break;
             
            default:
            break;
            }
         }                  
      //} 
//
      if (((stav[status1] & (je_audio|je_pc|je_pult))==0) & (servis==0))
            {
            switch (control_ach)
               {
               case 0:    
               break;            
            
               case 1:                  
               MOTOROLA=1; 
               wait3=CAS_MOTOROLA;      //3 [sec]
               control_ach=2;
//               
               stav[status1] |= ready;
 
               tx_buffer[p_STX]=STX;
	            tx_buffer[p_ADRESA_KAM]=ACH;
	            tx_buffer[p_ADRESA_ODKUD]=Z_DESKA;
	            tx_buffer[p_DATA1]=0;
	            tx_buffer[p_DATA2]=inpol[p_DATA2]; 
               tx_buffer[p_DATA3]=stav[povoleni];
	            tx_buffer[p_DATA4]=stav[status1];
               tx_buffer[p_DATA5]=stav[status2];
	            tx_buffer[p_DATA6]=0;
               tx_buffer[p_DATA7]=0;
	            tx_buffer[p_ETX]=ETX;
               tx_buffer[p_KSUMA]=0;
//                
               serial2();                    
//                              
               break;    
                                 
               case 2:
               if (wait3==0)
                  {
                  pomo_pol[0]=KOD_APLIKACE;	// kod aplikace
		            pomo_pol[1]=inpol[p_DATA2];   //cislo hlasice
		            pomo_pol[2] = 0;
                  pomo_pol[3] = set_hlasic;
                  pomo_pol[4] = 0;
                  pomo_pol[5] = 0;
                              
                  for (j=0;j<= 9;j++)
                     pol[j]=0;
       
                  prev_t(pol,pomo_pol,POBIT);    
                  
                  NOP;     //aaaaaaaaaaaaaaaaa
                 
                  PTT=1;
                  wait3=10;
                  control_ach=3;                             
                  NOP;
                  }
               break;      
            
               case 3:
               if (wait3==0)
                  {
                  cykl=1;                 
                  b_doba_vysilani=1;
                  doba_vysilani= DOBA_VYSILANI;   //* 100 [ms]                   
                  control_ach=4;
                  }
               break;
                    
               case 4:
               if (periferie)
                  { 
                  periferie=0;
                  wait3=10;
                  control_ach=5;
                  }
               break;
                                                
               case 5:
               if (wait3==0)
                  {                    
                  ASSR=0x00;        // povoleni PWM
                  TCCR2=0x06;
                  TCNT2=0x00;
                  OCR2=0x00;
                  
                  TIMSK=0x41;  
            
                  GICR = 0;
                  MCUCR=0;
                  MCUCSR=0;
                  GIFR=0;
                 
                  povol_pwm=1;
                     
                  RELE_PC=1;
                  wait3=10;
                  control_ach=6;
                  }
               break;
                        
               case 6:                 
               if (wait3==0)
                  { 
                  stav[status2] |= vysilani;
//                  
                  tx_buffer[p_STX]=STX;
	               tx_buffer[p_ADRESA_KAM]=ACH;
	               tx_buffer[p_ADRESA_ODKUD]=Z_DESKA;
	               tx_buffer[p_DATA1]=0;
	               tx_buffer[p_DATA2]=0; 
                  tx_buffer[p_DATA3]=0;
	               tx_buffer[p_DATA4]=0;
                  tx_buffer[p_DATA5]=0xa0;
	               tx_buffer[p_DATA6]=0;
                  tx_buffer[p_DATA7]=0;
	               tx_buffer[p_ETX]=ETX;
                  tx_buffer[p_KSUMA]=0;
//	               
                  serial2();                                            
//        
                  control_ach=7;
                  }            
               break;     
               
               case 7:
               if (!povol_pwm)
                  {             
                  if (b_doba_vysilani==0)
                     { 
                     povol_pwm = 1; 
                     control_ach=8;                         
                     }
                  }                    
               break;    
               
               case 8:
               break;

               default:
               break;
               }
         }           
//      
      if (xwait==0)
         {
         led = !led;  
         led1 = !led1;  
         
         if (cykl==0) 
            {
            if (blik_zprava)
               {
               LED_RUN = 0;                           
               }            
            else
               {
               LED_RUN=led;  
               xwait=5;
               }            
            }      
         }     
//          
      if (zprava)
		   {           
         zprava=0;  
         
         blik_zprava = 1;
         cas_blik_zprava = 20;           
         xwait=1;
         
         if (je_audio==0)
            {
            je_zprava = 1;           //uprava !!!!!!!
            }
		 
		   for (j=0;j<=K_ZPRAV;j++)
			   vysl[j]=0;

         vst_maska=vyst_maska=0x01;
         ukaz_vst=ukaz_vyst=chyba=0;

         for (j=0;j<=POBIT_VST;j++)   
		      {
            if (pol[ukaz_vst]&vst_maska)
               {
				   vst_maska <<= 1;
				   if (vst_maska==0)
				      {
				      vst_maska=0x01;
					   ukaz_vst++;
                  }

               if (pol[ukaz_vst]&vst_maska)
                  chyba++;				// 2x1
            else vysl[ukaz_vyst] |= vyst_maska;	//z�pis "1"
            }
		    else
			     {
			     vst_maska <<= 1;
			     if (vst_maska==0)
				      {
                  vst_maska=0x01;
				      ukaz_vst++;
                  }

			     if (pol[ukaz_vst]&vst_maska)
				     ;                  //zapis "0"
			     else	chyba++;				// 2x0
			     }

		      vst_maska <<= 1;      //dalsi test
		      if (vst_maska==0)
		        {
		        vst_maska=0x01;
			     ukaz_vst++;
              }

		      vyst_maska <<= 1;
		      if (vyst_maska==0)
		         {
               vyst_maska=0x01;
			      ukaz_vyst++;
		         }
          }                  
//     
      schov = j;
      NOP;    
         
		   {  
         NOP;                
         if (((vysl[1]&0x80)==0) /*&((vysl[3]&0x10)==0)*/ )
            {  
            for (j=0;j<=POCET_HLASICU-1;j++)
               {     
               aaa = vysl[1];
               bbb = cislo_hlasice[j];
               
               NOP;
               
               if ((vysl[1] & 0x7f) == cislo_hlasice[j]) 
                  {
                  pocet_zprav ++;                 
                  b_pocet_zprav=1;
                  cas_pocet_zprav=90;     // *100 [ms]                  
                  } 	  
               } 
                   
          if (pocet_zprav==2)
                  {                   
                  cas_blok_zpravy=5;
                  b_cas_blok_zpravy=1;
                                                                                     
                  zapis_zaznamu = E_zapis_zaznamu; 
                  
                  WAIT_EEPROM;   

                  cas_vypnuti=90;  // *100[ms]
                  b_cas_vypnuti=1;    
                  
                  for (j=0;j<=15;j++)       //poplach
                     tx_buffer[j]=0;    
    
                  tx_buffer[p_DATA2] = vysl[1]&0x3f;                  
                  tx_buffer[p_DATA9] = vysl[2];
                  tx_buffer[p_DATA10] = vysl[3];  
                                                         
                  gsm_ram.cislo_hlasice_prijate = vysl[1];
                  gsm_ram.byte1  =  vysl[2];
                  gsm_ram.byte2  =  vysl[3];     
                  
                  #asm("cli");                                                 
                  rtc_get_time(0,&ho,&mi,&se,&hs);
                  NOP;                                                     
                  rtc_get_date(0,&da,&mo,&ye);
                  #asm("sei");   
                  
                  tx_buffer[p_DATA3] =  ho;
                  tx_buffer[p_DATA4] =  mi;
                  tx_buffer[p_DATA5] =  se;
                  tx_buffer[p_DATA6] =  da;
                  tx_buffer[p_DATA7] =  mo;
                  tx_buffer[p_DATA8] =  ye-2000;      
                                                       
                  gsm_control = 1;    
                                              
                  if ((vysl[3]&0x08)==0x08)
                     {
                     serial1(STAV_HOVORU);                     
                     }
                  else                    
                     {     
                     binary = 1;
                                                                                
                     if (set_odpoved)
                        {  
                        set_odpoved = 0;   
                        wait_binary = 10;
                                                                  
                        serial1(ODPOVED_HLASIC);  

                        b_prisla_odpoved = 1;                                                                                
                        NOP;             
                        }                                   
                     else
                        {   
                        wait_binary = 2;
                                                                                                   
                        serial1(POPLACH);                                                                      
                        NOP;     
                                                                                                                            
                        zaznam[zapis_zaznamu].hod = ho;
                        WAIT_EEPROM;
                 
                        zaznam[zapis_zaznamu].min = mi;
                        WAIT_EEPROM;
                 
                        zaznam[zapis_zaznamu].sec = se;
                        WAIT_EEPROM;   
                 
                        zaznam[zapis_zaznamu].den = da;
                        WAIT_EEPROM;
                  
                        zaznam[zapis_zaznamu].mesic = mo;
                        WAIT_EEPROM;
                     
                        zaznam[zapis_zaznamu].rok = ye - 2000;     
                        WAIT_EEPROM;
                  
                        zaznam[zapis_zaznamu].cislo_hlasice = vysl[1]&0x3f;    
                        WAIT_EEPROM;                                           
                  
                        pomo1= vysl[1]&0x3f;
                  
                        zaznam[zapis_zaznamu].byte1 = vysl[2];                  
                        WAIT_EEPROM;                           
                        zaznam[zapis_zaznamu].byte2 = vysl[3] & 0x7;                  
                        WAIT_EEPROM; 
               
                        if (++ zapis_zaznamu ==   MAX_ZAZNAM)
                           zapis_zaznamu = 0;    
                         
                        E_zapis_zaznamu = zapis_zaznamu;
                        WAIT_EEPROM; 
                        WAIT_EEPROM; 
                        WAIT_EEPROM;  
                        NOP; 
                        }                         
                     }                                                         
                  }                                                                                                
               }
            }
        }            
//
      switch (binary)
         {
         case 0: 
         /* if (wait_binary==0)
            { 
            wait_binary = 3;
            binary = 2;
            }  
            */
         break;     
         
         case 1:
         if (wait_binary==0)
            {              
            for (j=0;j<=15;j++)       //poplach
               tx_buffer[j]=0;                  
               
            tx_buffer[p_STX]=STX;
	         tx_buffer[p_ADRESA_KAM]=PC;
	         tx_buffer[p_ADRESA_ODKUD]=DESKA_ODPOVED;                                      
//              
            tx_buffer[p_DATA1] = BINARY;
            tx_buffer[p_DATA2] = vysl[1] & 0x3f;    // cislo hlasice
//
            posun =  vysl[3];        //ADC0
            posun >>= 4;
            posun &= 0x0f;
            tx_buffer[p_DATA3]=posun;  
//            
            posun =  vysl[4];
            posun <<= 4;
            posun &= 0xf0;
            tx_buffer[p_DATA3] |= posun;
//
            posun=vysl[4];
            posun >>= 4;
            posun &= 0x03;
            tx_buffer[p_DATA4]=posun;  
//
            posun =  vysl[4];         //ADC1
            posun >>= 6;
            posun &= 0x03;
            tx_buffer[p_DATA5]=posun;   
//          
            posun =  vysl[5];
            posun <<= 2;
            posun &= 0xfc;
            tx_buffer[p_DATA5] |= posun;
//
            posun =  vysl[5];
            posun >>= 6;
            posun &= 0x03;
            tx_buffer[p_DATA6] |= posun;
//    
            tx_buffer[p_DATA7] = vysl[6];  //AKU
//           
            tx_buffer[p_DATA8] =  vysl[7]&0x03;
//          
	         tx_buffer[p_ETX]=ETX;
            tx_buffer[p_KSUMA]=0;   
//          
            serial2();
            binary=0;
            wait_binary=5;
            cas_wd = 50;
            b_cas_wd=1;
            }
         break;            
         
         case 2:         
//       while (binary);        //WD            
         break;        
//         
         default:
         break;
         }
//
      if ((stav[status1] & (je_audio|je_pc|je_pult))==0)
         {
         switch (control_odpoved)
            {
            case 0:    
            break;            
            
            case 1:
            MOTOROLA=1; 
            wait3=CAS_MOTOROLA;      //3 [sec]
            control_odpoved=2;               
            break;    
                                 
            case 2:
            if (wait3==0)
               {        
               for (j=0;j<= 9;j++)
                  pol[j]=0;    
    
               prev_t(pol,pomo_pol,POBIT);       // data nastavena v  DESKA_ODPOVED  
  
               PTT=1;
               wait3=10;
               control_odpoved=3;
               }
            break;      
            
            case 3:
            if (wait3==0)
               {
               cykl=1;                 
               b_doba_vysilani=1;
               doba_vysilani= DOBA_VYSILANI;   // 100 [ms]                   
               control_odpoved=4;
               }
            break;
                    
            case 4:
            if (b_doba_vysilani==0)
               {                 
               wait3=10;
               control_odpoved=5; 
               //povol_pwm=0;     
               GICR|=0x80;
               MCUCR=0x04;
               MCUCSR=0x00;
               GIFR=0x80;
               TIMSK=0x01; 
               }
            break;
                                                              
            case 5:                 
             if (wait3==0)
               { 
               for (j=0;j<=19;j++)
                  tx_buffer[j]=0;                                  
                           
               tx_buffer[p_DATA2] = posl_cislo_hlasice;   
               serial1(ODESLANO_HLASIC);
               
               wait3=10;      //1 [sec]
               control_odpoved=6;     
               
               set_odpoved=1;    
               GICR|=0x80;
               MCUCR=0x04;
               MCUCSR=0x00;
               GIFR=0x80;
               TIMSK=0x01;       
               }            
            break;
             
            case 6:
             if (wait3==0)
               {
               PTT=0;                             
               wait3=20;
               control_odpoved=7;    
               }            
            break;       
            
            case 7:
             if (wait3==0)
               {
               MOTOROLA=0;                
               wait3=100;     //cekani na timeout 70 * 100 [ms]
               control_odpoved=8;    
               }       
            break;
 
            case 8:
            if (wait3==0)     //timeout
               {                               
               for (j=0;j<=19;j++)
                  tx_buffer[j]=0;                                  
                           
               tx_buffer[p_DATA2] = posl_cislo_hlasice;     
               serial1(CHYBA_KOMUNIKACE);               
               control_odpoved=0; 
               NOP;
               }
            else
               {
               if (b_prisla_odpoved)
                  {  
                  b_prisla_odpoved=0;
                                                       
                  //wait3=50; 
                  control_odpoved=0;
                  NOP; 
                  }
               }               
            break;    
            
            case 9:
            while (control_odpoved);   //WD
            
            break;
                   
            default:
            break;
            }
         }   
//
      switch (gsm_control)
         {
         case 0:
         break;
         
         case 1: 
         
         aa = gsm_ram.stav;
         bb = gsm_ram.byte2;  
         cc = gsm_ram.cislo_hlasice;
         dd = gsm_ram.cislo_hlasice_prijate;     
         ee = gsm_ram.typ;
         ff = gsm_ram.byte1;
                
         if (gsm_ram.cislo_hlasice  ==   gsm_ram.cislo_hlasice_prijate)
            {                             
            switch  (gsm_ram.typ)
               {
               case  ANALOG0:     
               if ((gsm_ram.stav &0x03) <= (gsm_ram.byte1 &0x03))   //porovnani hodnot
                  {
                  if ((gsm_ram.stav & ODESLANA_SMS) == 0)
                     gsm_control=2;  
                  else gsm_control=0;
                  }
               else
                  {
                  if ((gsm_ram.byte1&0x03) <=  (gsm_ram.stav &0x03))   
                     {
                     gsm_ram.stav &= ~ODESLANA_SMS;  
                     gsm_control=0;
                     }     
                   else gsm_control=0;
                  }                                 
               break;
                  
               case  ANALOG1:   
               pomo =  gsm_ram.byte1;
               pomo >>= 2;
                                    
               if ((gsm_ram.stav &0x03) <= (pomo &0x03))   //porovnani hodnot
                  {
                  if ((gsm_ram.stav & ODESLANA_SMS) == 0)
                     gsm_control=2; 
                   else gsm_control=0;
                  }
               
               else
                  {
                   if ((pomo &0x03) <= (gsm_ram.stav&0x03))    
                     {
                     gsm_ram.stav &= ~ODESLANA_SMS; 
                     gsm_control=0;
                     }
                    else gsm_control=0;
                  }     
               break;
                  
               case  AKU: 
               pomo =  gsm_ram.byte1;
               pomo >>= 4;
                                    
               if ((gsm_ram.stav &0x03) >= (pomo &0x03))   //porovnani hodnot
                  {
                  if ((gsm_ram.stav & ODESLANA_SMS) == 0)
                     gsm_control=2;
                   else gsm_control=0;
                  }
               else
                  {
                    if ((gsm_ram.stav&0x03) <= (pomo &0x03))    //prijata
                     {
                     gsm_ram.stav &= ~ODESLANA_SMS;  
                     gsm_control=0;
                     } 
                     else gsm_control=0;
                  }     
               break;
                           
               case LOGIN1:                 
               aa = gsm_ram.stav;
               bb = gsm_ram.byte2;  
               cc = gsm_ram.cislo_hlasice;
               dd = gsm_ram.cislo_hlasice_prijate;     
               ee = gsm_ram.typ;
               ff = gsm_ram.byte1;
               
               if ((gsm_ram.stav &0x01) == (gsm_ram.byte2 &0x01))
                  {
                  if ((gsm_ram.stav & ODESLANA_SMS) == 0)
                     {
                     gsm_control=2;
                     }
                   else {
                        gsm_control=0;
                        }
                  }
               else
                  {
                  if ((gsm_ram.byte2&0x01) == 0)    //prijata
                     {
                     gsm_ram.stav &= ~ODESLANA_SMS;  
                     gsm_control=0;
                     }     
                   else 
                        {
                        gsm_control=0;
                        }
                  }     
               break;
                  
               case LOGIN2:  
               pomo =  gsm_ram.byte2;
               pomo >>= 1;
                  
               if ((gsm_ram.stav &0x01) == (pomo&0x01))
                  {
                  if ((gsm_ram.stav & ODESLANA_SMS) == 0)
                     gsm_control=2;  
                   else gsm_control=0;
                  }
               else
                  {
                  if ((gsm_ram.byte2&0x02) == 0)    //prijata
                     {
                     gsm_ram.stav &= ~ODESLANA_SMS;   
                     gsm_control=0;
                     } 
                   else gsm_control=0;
                  } 
               break; 
                  
               case OTEV_ROZV:   
               pomo =  gsm_ram.byte1;
               pomo >>= 6;
                  
               if ((gsm_ram.stav &0x01) == (gsm_ram.byte2 &0x01))
                  {
                  if ((gsm_ram.stav & ODESLANA_SMS) == 0)
                     gsm_control=2; 
                   else gsm_control=0;
                  }
               else
                  {
                  if ((gsm_ram.byte2&0x40) == 0)    //prijata
                     {
                     gsm_ram.stav &= ~ODESLANA_SMS;  
                     gsm_control=0;
                     }
                   else gsm_control=0;
                  }                   
               break; 
            
               case DOBIJENI:
               pomo =  gsm_ram.byte1;
               pomo >>= 7;
                  
               if ((gsm_ram.stav &0x01) == (gsm_ram.byte2 &0x01))
                  {
                  if ((gsm_ram.stav & ODESLANA_SMS) == 0)
                     gsm_control=2;  
                   else gsm_control=0;
                  }
               else
                  {
                  if ((gsm_ram.byte2&0x80) == 0)    //prijata
                     {
                     gsm_ram.stav &= ~ODESLANA_SMS;  
                     gsm_control=0;
                     }  
                   else gsm_control=0;
                  }     
               break; 
               }                                       
         break;  
         
         case 2:
         PIN_GSM=1;          //blokace pro ladeni
         cas_gsm = 20;         //  15*100[ms]    
         gsm_ram.stav |= ODESLANA_SMS;
         //         
         gsm_eeprom.stav =   gsm_ram.stav;
         WAIT_EEPROM;
         gsm_control = 3;
         break;
         
         case 3:   
         if (cas_gsm==0)
            {            
            PIN_GSM=0; 
            gsm_control=0; 
            }
         break;
         
         default:
         break; 
      } 
   gsm_control=0;         
   }                                                
//         
      if (set_100ms)
         {
         set_100ms=0;       
         



         if (xwait)
            xwait--;  
      
         if (wait2)
            wait2--;   
         
         if (wait3)
            wait3--;       
            
         if (wait_binary)
            wait_binary--; 
         
         if (wait_control)
            wait_control--; 
            
         if (cas_obnovy)
            cas_obnovy--;      
            
         if (cas_telefonu)
            cas_telefonu--;   
            
         if (cas_kontroly)
            cas_kontroly--;   
            
         if (cas_gsm)
            cas_gsm--;     
          
         if (cas_nouze)
            cas_nouze--;   
            
         if (cas_telefon)
            cas_telefon--;     
            
         if (cas_control_audio)
            cas_control_audio--;      
            
         if (blik_zprava)
            {   
            if (cas_blik_zprava)
               {
               cas_blik_zprava--;
               }                 
            else
               {
               blik_zprava=0;
               } 
            }
            
            
         if (b_doba_vysilani)
            {
            if (--doba_vysilani==0)
               {
               end_cykl=1;   
               
               if (servis)
                  ;
               else  
                  {
                  periferie=1;
                  }  
               
               b_doba_vysilani=0;
               }
            }  
//            
          if (b_cas_vypnuti)
            {
            if (cas_vypnuti)
               cas_vypnuti--;
            else
               {
               b_cas_vypnuti= pocet_zprav = 0;    
               blok_nul=zprava=start=0;   
                               
               start=ukaz=xzmena=ukaz_vst=ukaz_vyst=0;
               maska=0x01;   
               vysl[0]=vysl[1]=vysl[2]=vysl[3]=0;  
               
               for (j=0;j<=15;j++)
                  {
                  pripol[j]=0;
                  pol[j]=0;
                  apol[j]=0;
                  }
                NOP;                 
                }   
             }
             
         if (b_cas_wd)
            {
            if (--cas_wd==0)
               {
               if  ((control|servis|control_ach|control_odpoved)==0)
                  {
                  while (b_cas_wd);       //WD
                  }               
               }            
            }    
                                          
//         
         if (b_doba_provozu)
            {
            if (--doba_provozu==0)
               {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
               //control=6;
               inpol[p_DATA3]=mazani;
               zmena=1;
               }
            }    
                
         if (cas_timeout_kontrola)
            cas_timeout_kontrola--;
                                   
         /*if (b_restart)
            {
            if (++hodina_restart == 36000)
               {
               hodina_restart = 0;
            
               if (++celkovy_restart == 24)
                  {                                
                  while (1);                    
                  }
               }
            }
         */        
                

         if (b_pocet_zprav)
            {
            if (--cas_pocet_zprav==0)
               {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
               b_pocet_zprav=0;  
               pocet_zprav=0;
               cas_pocet_zprav = 0;
               }
            } 
            
         if (++po_sec == 10)
            {
            po_sec = 0;   
      
            sek=1;
       
            if (sekunda)
               sekunda--;             
            }                  
         } 
         
      if (sek)
         {
         sek=0;     
//                  
         if (b_cas_blok_zpravy)
            {
            if (--cas_blok_zpravy == 0)
               {
               b_cas_blok_zpravy=0;            
               }
            }        
//                  
         zmena_casu();    
//         
         if (++cas_obnova == 255)
            {
            cas_obnova=0;     

            gsm_ram.cislo_hlasice =  gsm_eeprom.cislo_hlasice;    
            WAIT_EEPROM;
            gsm_ram.typ  =  gsm_eeprom.typ;
            WAIT_EEPROM;               
            gsm_ram.stav =  gsm_eeprom.stav;
            WAIT_EEPROM;                                  
            } 
//
         } 
//         
      switch (control_audio)
            {
            case 0:
            break;
            
            case 1:
            b_doba_provozu=0; 
            servis=0; 
            
             povol_pwm = 0;  
            
            EXT_MOD=RELE_PC=RELE_MIC=RELE_TEL=0;
            RELE_AUDIO=MODULACE_DRAT=0;     
            
            servis = control_ach = 0;
            //ZNELKA=RESET;
              if (sm_gong)
               {   
               sm_gong=5;           
               wait2=2;
               }         
               
            if (sm_gongtest)
               {   
               sm_gongtest=5;           
               wait2=2;
               }      
                        
            POVOL_TEL=1;   
            //
              for (j=0;j<=ukaz_tel;j++)
                  {
                  pol_tel[j]=0;                  
                  }            
                                               
               cykl_tel=0;                    
               tel_zm1 = tel_zm_vst = tel_zm_star = tel_zm0 = 0;
               pocet_prvni_puls = 0;
               ukaz_tel = 0;
               stav[status1] &= ~je_telefon;               
            //
                    
               pomo_pol[0]=KOD_APLIKACE;	// kod aplikace
		         pomo_pol[1]=0;
		         pomo_pol[2]=0;
               pomo_pol[3] = 0;
               pomo_pol[4] = 0;
               pomo_pol[5] = 0;
                                           
               for (j=0;j<= 9;j++)
                  pol[j]=0;     
                                    
               prev_t(pol,pomo_pol,POBIT);  
               NOP;                                            
//               
               povol_pwm = 0;   
               //wait_control=30;
               b_doba_vysilani=1;
               doba_vysilani= DOBA_VYSILANI; //6 [s]
               cykl=1;
               control_audio=2;    
               control=0;
               break;                  
            
            case 2:
             if (periferie)
               {
               periferie=0;
               PTT=0;
               control_audio=3;
               cas_control_audio=20;  
               }                        
             break;         
             
             case 3:             
             if (cas_control_audio==0)
               {               
               in[0].stav=0xff; 
               in[1].stav=0xff;
//              
               control_audio=0;
               stav[status1] = je_audio | ready;  
//          
               control=2;  
                     
                doba_provozu=36000;  //60 [min]
               b_doba_provozu=1;               
               }              
            break;            
            }                                     
//          
      } //while

}       //main






