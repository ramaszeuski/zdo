
#define  NOP    #asm("nop"); #asm("nop"); #asm("nop");#asm("nop");#asm("nop");

//-----------------------------------------------------
//upravit pro ka�dou aplikaci
                                           
//ROR 2
#define KOD_APLIKACE    66
#define KOD_APLIKACE_ODPOVED    KOD_APLIKACE-1

#define CAS_MOTOROLA    45  //plati pro  verzi s rozvadecem R1
//#define CAS_MOTOROLA    2  //plati pro  verzi s rozvadecem R3

#define DOBA_VYSILANI   85  //pro retranslaci
//#define DOBA_VYSILANI   60    //bezne vysilani

#define DEF_ROZVADEC_R3 1

//-----------------------------------------------------------------------------


//definice p5enosu
#define PULS_1_MIN  40  //50
#define PULS_1_MAX  90  //90
#define PULS_2_MIN  120 //120
#define PULS_2_MAX  170 //165
#define PULS_3_MIN  190 //190
#define PULS_3_MAX  240 //230
#define PRVNI_1      60 //60

#define MAX_ZAZNAM            110           //113

//#define POBIT           22	   //pocet bitu urcenych k prevodu
//#define VYS_POBIT		   46	   //pocet bitu urcenych k vysilani

//ROR 2014
#define POBIT           27	   //pocet bitu urcenych k prevodu
#define VYS_POBIT		   60	   //pocet bitu urcenych k vysilani

//vstup  A/D 8 bit A/D
//#define  POBYTE_VST     14   //pocet byte vyst.pole
//#define  POBIT_VST      114   //21   //pocet byte vyst.pole
//#define  D_ZPRAV        16
//#define  K_ZPRAV        8

//verze z 11/2014
//vstup A/D 10 bit
#define  POBYTE_VST     16   //pocet byte vyst.pole
#define  POBIT_VST      127   //125
#define  D_ZPRAV        16
#define  K_ZPRAV        19

//casovace
#define CAS_PULSU  	   8
#define DOBA_ZNELKY     195
#define CAS_NOUZE  	   1800
#define CAS_100MS       100 
#define CEKEJ     cekej= 20; while(--cekej)
// 

#define SET    0
#define RESET  1

#define MS 12

#define STX 2
#define ETX 3
 
//data zpr�vy z  PC
#define p_STX           0 
#define p_ADRESA_KAM    1
#define p_ADRESA_ODKUD  2
#define p_DATA1         3
#define p_DATA2         4
#define p_DATA3         5
#define p_DATA4         6
#define p_DATA5         7
#define p_DATA6         8
#define p_DATA7         9
#define p_DATA8        10
#define p_DATA9        11
#define p_DATA10       12
#define p_ETX          13 
#define p_KSUMA        14 

//periferie
#define  PC                0x80
#define  Z_DESKA           0x40
#define  PULT              0x20
#define  AUDIO             0x10
#define  ACH               0x08
#define  VSEM              0x04
#define  SERVIS            0x02
#define  DESKA_ODPOVED     0x01

//p_DATA3
#define  mazani            0x01
#define  general           0x02
#define  mikrofon          0x04
#define  ext_modulace      0x08
#define  znelka            0x10
#define  konec_sireny      0x20
#define  ovl_sireny        0x40
#define  jsvv_T9           0x80

#define MAZANI       ((inpol[p_DATA3]&mazani)==mazani)
#define GENERAL      ((inpol[p_DATA3]&general)==general)
#define MIKROFON     ((inpol[p_DATA3]&mikrofon)==mikrofon)
#define JE_EXT_MOD   ((inpol[p_DATA3]&ext_modulace)==ext_modulace)
#define JE_ZNELKA    ((inpol[p_DATA3]&znelka)==znelka)
#define KONEC_SIRENY ((inpol[p_DATA3]&konec_sireny)==konec_sireny)
#define POV_SIRENY   ((inpol[p_DATA3]&pov_sireny)==pov_sireny)
#define JSVV         ((inpol[p_DATA3]&jsvv_T9)==jsvv_T9)

//p_DATA4
#define  je_pc             0x01
#define  je_pult           0x02
#define  je_audio          0x04
#define  je_telefon        0x08
#define  je_test           0x10
#define  test_rezim        0x20
#define  test              0x40
#define  ready             0x80

#define  je_ach            0x01

#define JE_PC        ((stav[status1]&je_pc)==je_pc)
#define JE_PULT      ((stav[status1]&je_pult)==je_pult)
#define JE_AUDIO     ((stav[status1]&je_audio)==je_audio)
#define JE_TELEFON   ((stav[status1]&je_telefon)==je_telefon)
#define JE_TEST      ((stav[status1]&je_test)==je_test)
#define TEST_REZIM   ((stav[status1]&test_rezim)==test_rezim)
#define TEST         ((stav[status1]&test)==test)
#define STAV_DESKY   ((stav[status1]&stav_desky)==stav_desky)

//p_DATA5
#define  start_ach         0x20
#define  kontrola          0x40
#define  vysilani          0x80

//p_DATA7 

//stav syst�mu
#define  povoleni    0
#define  status1     1
#define  status2     2
#define  rezerva     3
#define  stav_servis 4
               
//porty
#define MZ_POWER        PORTA.0
#define JP4_SMS         !PINA.1
#define JP3_MOTOROLA    !PINA.2
#define JP2_DRAT        !PINA.3
#define JP1_MZ          !PINA.4
#define MOTOROLA        PORTA.5
#define PTT             PORTA.6
#define MODULACE_DRAT   PORTA.7

#define PL1             PORTB.0 
#define PL2             PORTB.1
#define LED_RUN         PORTB.2
#define EXT_MOD         PORTB.3
#define ZNELKA          PORTB.4
#define PIN_GSM         PORTB.5
#define ZADOST_TEL      PINB.6
#define POVOL_TEL       PORTB.7

#define RELE_AUDIO      PORTC.0
#define RELE_TEL        PORTC.1
//#define TCK           PORTC.2
//#define TMS           PORTC.3
//#define TD0           PORTC.4
//#define TDI           PORTA.5
#define RELE_MIC        PORTC.6
#define RELE_PC         PORTC.7

#define  RX             PIND.0
#define  TX             PORTD.1
#define  RTS            PORTD.2 
#define VSTUP_DATA      PIND.3
//#define                 PORTD.4
//#define                 PORTD.5
//#define TESTIK          PORTD.6
#define  POWER_DRAT     PORTD.7

//servisni_rezim
#define   start_servis  0x01
#define   aservis          0x09

// ZPRAVY Z PC do z�kladn� desky  -  oboustrann� hl�si�
#define PC_INFO                  1
#define ODPOVED_HLASIC           2
#define CHYBA_KOMUNIKACE         3
#define SET_RELE                 4
#define RESET_RELE               5
#define CTENI_EEPROM             6
#define NASTAVENI_CASU           7
#define CHYBA_KS                 8
#define KONEC	                  9
#define POPLACH_Z_HLASICE       10
#define ZPRAVA_ODESLANA         11
#define PRIJATA_ZPRAVA_CAS      12
#define PRIJATA_ZPRAVA_INFO     13
#define ODESLANO_HLASIC         14
#define POPLACH                 15
#define SET_GSM                 16
#define GET_GSM                 17
#define NENI_EEPROM             18
#define CTENI_CASU              19
#define STAV_HOVORU             20 
#define ROZVADEC_R3             21
#define PC_HOVOR_START          22
#define PC_HOVOR_STOP           23
#define BINARY                  24 

#define VYSILANI TCCR1A=0x00;TCCR1B=0x04;ASSR=0x00;TCCR2=0x61;b_prijem=0;
#define PRIJEM    TCCR1A=0x00;b_prijem=1;

#define WAIT_EEPROM wait_eeprom=15;while (wait_eeprom)

//porovnani analogu
#define  nepouzito        0
#define  stav_chyba     0x01
#define  mezistav       0x02
#define  stav_ok        0x03

//definice GSM hlasice      / stav
#define  ZATRZENO       0x80
#define  ODESLANA_SMS   0x40

//definice GSM hlasice      / typ
#define  ANALOG0       0x01
#define  ANALOG1       0x02
#define  AKU           0x04
#define  LOGIN1        0x08
#define  LOGIN2        0x10
#define  OTEV_ROZV     0x20
#define  DOBIJENI      0x40     

//volba rezimu
#define  infra          0x01
#define  odpoved        0x02
#define  set_hlasic     0x04 
  
